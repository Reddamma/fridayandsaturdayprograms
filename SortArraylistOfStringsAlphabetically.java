
//write a Program to sort Arraylist of strings alphabetically

import java.util.ArrayList;

public class SortArraylistOfStringsAlphabetically {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		String temp;
		
		ArrayList<String> str = new ArrayList<String>();
		
		str.add("Orange");
		str.add("Apple");
		str.add("Cipla");
		str.add("Banana");
		
		for (int i=0;i<str.size();i++) {

			for (int j=i+1;j<str.size();j++) {

				if (str.get(i).compareTo(str.get(j))>0){

					temp = str.get(i);
					str.set (i,str.get(j));
					str.set (j,temp);

				}
			}
		}

		System.out.println("List of words in alphabetical order   " + str);

	}
}
