
//Write a program two numbers without using third number.

import java.util.*;
import java.util.Scanner;

public class SwapTwoNumbers {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println("enter the values of x and y");
		Scanner sc = new Scanner(System.in);
		// Define variables
		int x = sc.nextInt();
		int y = sc.nextInt();
		System.out.println("before swapping: " + x + " " + y);
		// Swapping
		x = x + y;
		y = x - y;
		x = x - y;
		System.out.println("After swapping: " + x + "  " + y);
	}

}
