package String;
import java.util.Scanner;

public class Uppercase {

				public static void main(String[] args)
				{
					
					Scanner sc=new Scanner(System.in);
					System.out.println("How many strings u want to check?");
					
					int t=sc.nextInt();
					while(t-->0){
						System.out.println("Enter the string!");
						String s=sc.next();
						int uppercase=0,lowercase=0;
						for(int i=0;i<s.length();i++){
							
							if(Character.isLowerCase(s.charAt(i))){
								lowercase++;
							}
							else if(Character.isUpperCase(s.charAt(i))){
								uppercase++;
							}
						}
					
						System.out.println("No. of lowercase letter : " + lowercase);
						System.out.println("No. of uppercase letter : " + uppercase);
						
						System.out.println();
					}
				}
}


